# scanproc

This is a tool to process high-resolution scans with as little user
input as possible. It produces decently compressed PDF files with
monochrome/color separation and more or less accurate text layers.

## Requirements

In addition to the Python modules listed in `requirements.txt`,
scanproc requires [jbig2](https://github.com/agl/jbig2enc) to
be installed in your `$PATH`.

## Usage

`scanproc.py [-h] [-l LANGS] [-d DPI] [--debug] output [imgs ...]`

Usage is simple. You pass a list of
[EasyOCR language codes](https://www.jaided.ai/easyocr/) to use
with `-l` (default English only; pass multiple times for more than
one language), the image resolution (required for filter kernel
scaling and correct page size), followed by the output PDF file and
the input image files.
